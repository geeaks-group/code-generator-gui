package com.geeaks.generator.controller;

import com.geeaks.generator.model.DatabaseConfigBo;
import com.geeaks.generator.util.ConfigHelper;
import com.geeaks.generator.util.DbUtil;
import com.geeaks.generator.view.AlertUtil;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @Description: 新建DB连接 控制器
 * @author gaoxiang
 * @date 2019-04-13 11:21:00
 */ 
public class NewConnectionController extends BaseFXController {
	
	private static final Logger logger = LoggerFactory.getLogger(NewConnectionController.class);
	
	@FXML
	private TextField nameField;
	
	@FXML
	private TextField hostField;
	
	@FXML
	private TextField portField;
	
	@FXML
	private TextField userNameField;
	
	@FXML
	private TextField passwordField;
	
	@FXML
	private TextField schemaField;
	
	@FXML
	private ChoiceBox<String> encodingChoice;
	
	@FXML
	private ChoiceBox<String> dbTypeChoice;
	
	private MainUIController mainUIController;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}
	
	@FXML
	void saveConnection() {
		DatabaseConfigBo config = extractConfigForUI();
		if (config == null) {
			return;
		}
		try {
			ConfigHelper.saveDatabaseConfig(config.getName(), config);
			getDialogStage().close();
			mainUIController.loadLeftDBTree();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			AlertUtil.showErrorAlert(e.getMessage());
		}
	}
	
	@FXML
	void testConnection() {
		DatabaseConfigBo config = extractConfigForUI();
		if (config == null) {
			return;
		}
		try {
			String url = DbUtil.getConnectionUrlWithSchema(config);
			System.out.println(url);
			DbUtil.getConnection(config);
			AlertUtil.showInfoAlert("连接成功");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			AlertUtil.showWarnAlert("连接失败");
		}
	}
	
	@FXML
	void cancel() {
		getDialogStage().close();
	}
	
	void setMainUIController(MainUIController controller) {
		this.mainUIController = controller;
	}
	
	private DatabaseConfigBo extractConfigForUI() {
		String name = nameField.getText();
		String host = hostField.getText();
		String port = portField.getText();
		String userName = userNameField.getText();
		String password = passwordField.getText();
		if ("".equals(password)) {
			password = "";
		}
		String encoding = encodingChoice.getValue();
		String dbType = dbTypeChoice.getValue();
		String schema = schemaField.getText();
		DatabaseConfigBo config = new DatabaseConfigBo();
		config.setName(name);
		config.setDbType(dbType);
		config.setHost(host);
		config.setPort(port);
		config.setUsername(userName);
		config.setPassword(password);
		config.setSchema(schema);
		config.setEncoding(encoding);
		if (StringUtils.isAnyEmpty(name, host, port, userName, encoding, dbType, schema)) {
			AlertUtil.showWarnAlert("所有字段都必填");
			return null;
		}
		return config;
	}
}
