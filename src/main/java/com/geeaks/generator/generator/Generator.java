package com.geeaks.generator.generator;

import java.io.File;
import org.springframework.util.ResourceUtils;
import com.geeaks.generator.model.DatabaseConfigBo;
import com.geeaks.generator.model.GenerateConfigBo;

public abstract class Generator {
	
	protected GenerateConfigBo config;
	
	protected DatabaseConfigBo dbConfig;
	
	protected static String prefix = null;
	
	public String generate() {
		String warn = null;
		try {
			prefix = ResourceUtils.getFile("classpath:").getPath();
			File file = ResourceUtils.getFile("classpath:template/" + config.getTemplatePath());
			File sourceDirectory = file;
			// 渲染
			renderTemplate(sourceDirectory);
		} catch (Exception e) {
			e.printStackTrace();
			warn = e.getMessage();
		}
		return warn;
	}
	
	protected abstract void renderTemplate(File sourceDirectory) throws Exception;
	
	public void setGeneratorConfig(GenerateConfigBo config) {
		this.config = config;
	}
	
	public void setDatabaseConfig(DatabaseConfigBo dbConfig) {
		this.dbConfig = dbConfig;
	}
}
