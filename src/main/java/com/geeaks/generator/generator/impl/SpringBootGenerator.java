package com.geeaks.generator.generator.impl;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import com.geeaks.generator.enums.EnumTypeTransfer;
import com.geeaks.generator.enums.PatternConst;
import com.geeaks.generator.generator.Generator;
import com.geeaks.generator.model.RowInfoBo;
import com.geeaks.generator.model.TableInfoBo;
import com.geeaks.generator.util.DbUtil;
import com.geeaks.generator.util.VelocityUtils;
import com.google.common.base.Splitter;
import com.google.common.collect.Maps;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

public class SpringBootGenerator extends Generator {
	
	@Override
	protected void renderTemplate(File sourceFile) throws Exception {
		// 目录的话 递归执行
		if (sourceFile.isDirectory()) {
			for (File file : sourceFile.listFiles()) {
				renderTemplate(file);
			}
		} else if (StringUtils.endsWith(sourceFile.getName(), ".vm")) {
			Map<String, Object> fillData = Maps.newHashMap();
			TableInfoBo tableInfo = DbUtil.getTableInfo(dbConfig, config.getTableNameField());
			/******** 渲染entity ********/
			renderEntity(fillData, tableInfo);
			/******** 渲染xml ********/
			renderXml(fillData, tableInfo);
			/******** 渲染dao ********/
			renderMapper(fillData, tableInfo);
			// 表注释
			fillData.put("tableDesc", tableInfo.getTableDesc());
			// entity里的字段
			fillData.put("javaField", config.getPackageNameField());
			fillData.put("tableNameField", config.getTableNameField());
			// 包路径
			fillData.put("package", config.getPackageNameField());
			// 类名
			fillData.put("className", config.getEntityField());
			// 作者
			fillData.put("author", "gaoxiang");
			// 当前日期
			fillData.put("nowDate", DateUtil.now());
			String sourcePath = StringUtils.removeStart(sourceFile.getPath(), prefix);
			String entityField = config.getEntityField();
			if(StringUtils.startsWith(sourceFile.getName(), "Common")) {
				entityField = "";
			}
			String targetPath = config.getTargetField() + StringUtils.removeEnd(sourcePath,sourceFile.getName()) + entityField + StringUtils.removeEnd(sourceFile.getName(), ".vm");
			targetPath = StringUtils.remove(targetPath, "/template/" + config.getTemplatePath());
			String packageName = StringUtils.replace(config.getPackageNameField(), ".", "/");
			String searchStr = "src/main/java";
			if(StringUtils.contains(targetPath, "src/main/resources")) {
				searchStr = "src/main/resources";
			} else if(StringUtils.contains(targetPath, "src/test/java")) {
				searchStr = "src/test/java";
			}
			targetPath = StringUtils.replace(targetPath, searchStr, ",");
			List<String> split = Splitter.on(",").splitToList(targetPath);
			if(StringUtils.equalsIgnoreCase(searchStr, "src/main/resources")) {
				targetPath = split.get(0) + searchStr + "/" + split.get(1);
			} else {
				targetPath = split.get(0) + searchStr + "/" + packageName + split.get(1);
			}
			FileUtils.write(new File(targetPath), VelocityUtils.getTemplate(sourcePath, fillData), StandardCharsets.UTF_8);
		}
	}
	
	private void renderMapper(Map<String, Object> fillData, TableInfoBo tableInfo) {
		StringBuilder importJavaType = new StringBuilder();
		for (RowInfoBo rowInfo : tableInfo.getTableColumns()) {
			EnumTypeTransfer typeTransfer = EnumTypeTransfer.codeOf(rowInfo.getType());
			if("id".equals(rowInfo.getField())) {
				fillData.put("idType", typeTransfer.javaType);
				importJavaType.append(StrUtil.format(PatternConst.importPattern, typeTransfer.javaTypeImport));
			}
			fillData.put("daoImport", importJavaType.toString());
			fillData.put("className2", StrUtil.toCamelCase(config.getTableNameField()));
		}
	}
	
	private void renderXml(Map<String, Object> fillData, TableInfoBo tableInfo) {
		StringBuilder column = new StringBuilder();
		StringBuilder selColumn = new StringBuilder();
		StringBuilder whereColumn = new StringBuilder();
		StringBuilder updateColumn = new StringBuilder();
		StringBuilder insertColumn = new StringBuilder();
		boolean hasId = false;
		for (RowInfoBo rowInfo : tableInfo.getTableColumns()) {
			if("id".equals(rowInfo.getField())) {
				hasId = true;
			}
			String camelCase = StrUtil.toCamelCase(rowInfo.getField());
			column.append(rowInfo.getField()).append(",");
			selColumn.append(rowInfo.getField() + " " + StrUtil.toCamelCase(rowInfo.getField())).append(",");
			whereColumn.append(StrUtil.format(PatternConst.wherePattern, camelCase,camelCase,rowInfo.getField(),camelCase));
			updateColumn.append(StrUtil.format(PatternConst.updatePattern, camelCase,camelCase,rowInfo.getField(),camelCase));
			insertColumn.append("#{").append(camelCase).append("},");
		}
		fillData.put("hasId", hasId);
		fillData.put("column", StringUtils.removeEnd(column.toString(), ","));
		fillData.put("selColumn", StringUtils.removeEnd(selColumn.toString(), ","));
		fillData.put("whereColumn", whereColumn.toString());
		fillData.put("updateColumn", updateColumn.toString());
		fillData.put("insertColumn", StringUtils.removeEnd(insertColumn.toString(), ","));
	}
	
	private void renderEntity(Map<String, Object> fillData, TableInfoBo tableInfo) {
		StringBuilder importJavaType = new StringBuilder();
		StringBuilder fields = new StringBuilder();
		StringBuilder setterGetter = new StringBuilder();
		Map<String, String> importmap = Maps.newHashMap();
		for (RowInfoBo rowInfo : tableInfo.getTableColumns()) {
			EnumTypeTransfer typeTransfer = EnumTypeTransfer.codeOf(rowInfo.getType());
			if (!importmap.containsKey(typeTransfer.javaType)) {
				importmap.put(typeTransfer.javaType, "");
				importJavaType.append(StrUtil.format(PatternConst.importPattern, typeTransfer.javaTypeImport));
			}
			String camelCase = StrUtil.toCamelCase(rowInfo.getField());
			fields.append(StrUtil.format(PatternConst.entityFieldPattern, camelCase, StringUtils.remove(rowInfo.getComment(), "'"), typeTransfer.javaType, camelCase));
		}
		fillData.put("fields", fields.toString());
		fillData.put("importJavaType", importJavaType.toString());
		fillData.put("setterGetter", setterGetter.toString());
	}
	
}
