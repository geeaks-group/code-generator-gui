package com.geeaks.generator.enums;

public enum EnumFXMLPage {
	new_connection("fxml/newConnection.fxml","新建连接"), 
	select_table_column("fxml/selectTableColumn.fxml","定制列"), 
	template("fxml/template.fxml","模板"),;
	
	public String fxml;
	
	public String desc;
	
	EnumFXMLPage(String fxml,String desc) {
		this.fxml = fxml;
		this.desc = desc;
	}
}
