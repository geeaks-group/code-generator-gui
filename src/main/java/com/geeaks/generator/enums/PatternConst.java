package com.geeaks.generator.enums;

/**
 * @Description: 通用代码常量模式
 * @author gaoxiang
 * @date 2019-04-15 18:40:24
 */ 
public class PatternConst {
	
	public static String importPattern = "import {};\n";
	
	public static String entityFieldPattern = "	/**\n" + 
			"	 * {}\n" + 
			"	 */\n" + 
			"	@SCFMember(orderId = {})\n" + 
			"	private {} {};\n	\n";
	
	public static String wherePattern = "			<if test=\"{} != null and {} != ''\"> and {} = #{{}} </if>\n";
	public static String wherePattern2 = "			<if test=\"{} != null\"> and {} = #{{}} </if>\n";
	
	public static String updatePattern = "			<if test=\"{} != null and {} != ''\"> {} = #{{}}, </if>\n";
	public static String updatePattern2 = "			<if test=\"{} != null\"> {} = #{{}}, </if>\n";
}
