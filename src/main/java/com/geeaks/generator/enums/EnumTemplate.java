package com.geeaks.generator.enums;

import java.util.List;
import com.geeaks.generator.generator.Generator;
import com.geeaks.generator.generator.impl.Classic3LayerGenerator;
import com.geeaks.generator.generator.impl.WubaGenerator;
import com.geeaks.generator.model.TemplateBo;
import com.google.common.collect.Lists;

/**
 * @Description: 模板配置枚举常量
 * @author gaoxiang
 * @date 2019-04-13 17:12:47
 */ 
public enum EnumTemplate {
	
	classic3Layer("classic3Layer","经典三层",Classic3LayerGenerator.class),
	wuba("wuba","58模板",WubaGenerator.class),
	;
	
	/**
	 * @Fields path : 模板目录路径
	 */ 
	public String path;
	
	/**
	 * @Fields desc : 描述
	 */ 
	public String desc;
	public Class<? extends Generator> clazz;

	private EnumTemplate(String path, String desc,Class<? extends Generator> clazz) {
		this.path = path;
		this.desc = desc;
		this.clazz = clazz;
	}
	
	public static List<TemplateBo> getList() {
		List<TemplateBo> list = Lists.newArrayList();
		for(EnumTemplate enumTemplate : EnumTemplate.values()) {
			TemplateBo templateBo = new TemplateBo();
			templateBo.setTemplateCode(enumTemplate.toString());
			templateBo.setTemplateName(enumTemplate.desc);
			list.add(templateBo);
		}
		return list;
	}
	
	public static EnumTemplate nameOf(String name) {
		for(EnumTemplate enumTemplate : EnumTemplate.values()) {
			if(enumTemplate.name().equals(name)) {
				return enumTemplate;
			}
		}
		return EnumTemplate.classic3Layer;
	}
	
	public Generator getGenerator() throws InstantiationException, IllegalAccessException{
		Generator generator = this.clazz.newInstance();
		return generator;
	}
}
