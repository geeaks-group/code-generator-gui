package com.geeaks.generator.enums;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ResourceUtils;
import com.google.common.base.Splitter;
import cn.hutool.core.util.StrUtil;

public enum EnumTypeTransfer {
	
	BIGINT("BIGINT","Long","Long","java.lang.Long"),
	TINYINT("TINYINT","Integer","Integer","java.lang.Integer"),
	SMALLINT("SMALLINT","Integer","Integer","java.lang.Integer"),
	MEDIUMINT("MEDIUMINT","Integer","Integer","java.lang.Integer"),
	INTEGER("INTEGER","Integer","Integer","java.lang.Integer"),
	INT("INT","Integer","Integer","java.lang.Integer"),
	FLOAT("FLOAT","Double","Double","java.lang.Float"),
	DOUBLE("DOUBLE","Double","Double","java.lang.Double"),
	DECIMAL("DECIMAL","BigDecimal","BigDecimal","java.math.BigDecimal"),
	NUMERIC("NUMERIC","BigDecimal","BigDecimal","java.math.BigDecimal"),
	CHAR("CHAR","String","String","java.lang.String"),
	VARCHAR("VARCHAR","String","String","java.lang.String"),
	DATE("DATE","Date","Date","java.util.Date"),
	DATETIME("DATETIME","Date","Date","java.util.Date"),
	TIMESTAMP("TIMESTAMP","Timestamp","Timestamp","java.sql.Timestamp"),
	Common(null,"String","String","java.lang.String"),
	;
	
	public String dbType;
	
	public String javaType;
	
	public String javaType2;
	
	public String javaTypeImport;
	
	private EnumTypeTransfer(String dbType, String javaType, String javaType2, String javaTypeImport) {
		this.dbType = dbType;
		this.javaType = javaType;
		this.javaType2 = javaType2;
		this.javaTypeImport = javaTypeImport;
	}
	
	public static EnumTypeTransfer codeOf(String dbType) {
		return Arrays.asList(EnumTypeTransfer.values()).stream().filter(type -> StringUtils.equalsIgnoreCase(dbType, type.dbType))
		.findFirst().orElse(Common);
	}
	
	public boolean needImport() {
		return this.javaType.equals(javaType2);
	}
	
	public static void main(String[] args) throws Exception {
		List<String> lines = FileUtils.readLines(ResourceUtils.getFile("classpath:leixing"),StandardCharsets.UTF_8);
		String template = "{}(\"{}\",\"{}\",{}),";
		for(String line : lines) {
			List<String> list = Splitter.on(",").splitToList(line);
			String javaTyp = list.get(1);
			Class<?> forName = null;
			String importType = null;
			try {
				forName = Class.forName("java.lang."+javaTyp);
				importType = "\""+forName.getPackage().getName()+"."+javaTyp+"\"";
			} catch (Exception e) {
				try {
					forName = Class.forName("java.math."+javaTyp);
					importType = "\""+forName.getPackage().getName()+"."+javaTyp+"\"";
				} catch (Exception e1) {
					try {
						forName = Class.forName("java.util."+javaTyp);
						importType = "\""+forName.getPackage().getName()+"."+javaTyp+"\"";
					} catch (Exception e2) {
						importType = null;
					}
					
				}
			}
			System.out.println(StrUtil.format(template, list.get(0), list.get(0), javaTyp, importType));
		}
	}
}
