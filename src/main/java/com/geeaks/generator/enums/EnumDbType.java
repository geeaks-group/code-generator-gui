package com.geeaks.generator.enums;

public enum EnumDbType {

	MySQL("com.mysql.jdbc.Driver", "jdbc:mysql://%s:%s/%s?useUnicode=true&useSSL=false&characterEncoding=%s", "mysql-connector-java-8.0.18.jar"),
	Oracle("oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@%s:%s:%s", "ojdbc14.jar"),
	PostgreSQL("org.postgresql.Driver", "jdbc:postgresql://%s:%s/%s", "postgresql-9.4.1209.jar");
	
	private final String driverClass;
	
	private final String connectionUrlPattern;
	
	private final String connectorJarFile;
	
	EnumDbType(String driverClass, String connectionUrlPattern, String connectorJarFile) {
		this.driverClass = driverClass;
		this.connectionUrlPattern = connectionUrlPattern;
		this.connectorJarFile = connectorJarFile;
	}
	
	public String getDriverClass() {
		return driverClass;
	}
	
	public String getConnectionUrlPattern() {
		return connectionUrlPattern;
	}
	
	public String getConnectorJarFile() {
		return connectorJarFile;
	}
}