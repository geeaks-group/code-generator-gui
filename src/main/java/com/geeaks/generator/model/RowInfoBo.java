package com.geeaks.generator.model;

/**
 * @Description: 表字段行信息
 * @author gaoxiang
 * @date 2019-04-15 16:36:06
 */
public class RowInfoBo {
	
	/**
	 * @Fields field : 字段名
	 */
	private String field;
	
	/**
	 * @Fields type : 字段类型
	 */
	private String type;
	
	/**
	 * @Fields length : 字段长度
	 */
	private Integer length;
	
	/**
	 * @Fields delimiterLength : 小数点长度
	 */
	private Integer delimiterLength;
	
	/**
	 * @Fields length : 是否可为空
	 */
	private Boolean nullAble;
	
	/**
	 * @Fields comment : 字段注释
	 */
	private String comment;
	
	public String getField() {
		return field;
	}
	
	public void setField(String field) {
		this.field = field;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public Integer getLength() {
		return length;
	}
	
	public void setLength(Integer length) {
		this.length = length;
	}
	
	public Boolean getNullAble() {
		return nullAble;
	}
	
	public void setNullAble(Boolean nullAble) {
		this.nullAble = nullAble;
	}
	
	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public Integer getDelimiterLength() {
		return delimiterLength;
	}
	
	public void setDelimiterLength(Integer delimiterLength) {
		this.delimiterLength = delimiterLength;
	}
}
