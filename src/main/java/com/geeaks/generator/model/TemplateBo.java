package com.geeaks.generator.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TemplateBo {
	
	private StringProperty templateCode = new SimpleStringProperty();
	
	private StringProperty templateName = new SimpleStringProperty();
	
	public String getTemplateCode() {
		return templateCode.get();
	}
	
	public void setTemplateCode(String templateCode) {
		this.templateCode.set(templateCode);
	}
	
	public String getTemplateName() {
		return templateName.get();
	}
	
	public void setTemplateName(String templateName) {
		this.templateName.set(templateName);
	}
}
