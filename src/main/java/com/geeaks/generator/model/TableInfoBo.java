package com.geeaks.generator.model;

import java.util.List;

public class TableInfoBo {
	
	/**
	 * @Fields tableName : 表名
	 */ 
	private String tableName;
	
	/**
	 * @Fields tableDesc : 表注释
	 */ 
	private String tableDesc;
	
	/**
	 * @Fields tableColumns : 字段列表
	 */
	private List<RowInfoBo> tableColumns;
	
	public String getTableName() {
		return tableName;
	}
	
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public String getTableDesc() {
		return tableDesc;
	}
	
	public void setTableDesc(String tableDesc) {
		this.tableDesc = tableDesc;
	}
	
	public List<RowInfoBo> getTableColumns() {
		return tableColumns;
	}
	
	public void setTableColumns(List<RowInfoBo> tableColumns) {
		this.tableColumns = tableColumns;
	}
}
