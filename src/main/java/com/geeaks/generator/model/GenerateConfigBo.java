package com.geeaks.generator.model;

import java.util.List;
import org.mybatis.generator.config.ColumnOverride;
import org.mybatis.generator.config.IgnoredColumn;

/**
 * @Description: 用户页面设置的生成参数业务对象
 * @author gaoxiang
 * @date 2019-04-13 19:59:35
 */
public class GenerateConfigBo {
	
	/**
	 * @Fields targetField : 目标目录
	 */
	private String targetField;
	
	private String packageNameField;
	
	private String tableNameField;
	
	private String entityField;
	
	/**
	 * @Fields author : 作者
	 */
	private String author;
	
	/**
	 * @Fields type : 项目 或者 单表
	 */
	private String type;
	
	/**
	 * @Fields templatePath : 模板路径
	 */
	private String templatePath;
	
	List<ColumnOverride> columnOverrides;
	
	List<IgnoredColumn> ignoredColumns;
	
	public String getTargetField() {
		return targetField;
	}
	
	public void setTargetField(String targetField) {
		this.targetField = targetField;
	}
	
	public String getPackageNameField() {
		return packageNameField;
	}
	
	public void setPackageNameField(String packageNameField) {
		this.packageNameField = packageNameField;
	}
	
	public String getTableNameField() {
		return tableNameField;
	}
	
	public void setTableNameField(String tableNameField) {
		this.tableNameField = tableNameField;
	}
	
	public String getEntityField() {
		return entityField;
	}
	
	public void setEntityField(String entityField) {
		this.entityField = entityField;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getTemplatePath() {
		return templatePath;
	}
	
	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}
	
	public List<ColumnOverride> getColumnOverrides() {
		return columnOverrides;
	}
	
	public void setColumnOverrides(List<ColumnOverride> columnOverrides) {
		this.columnOverrides = columnOverrides;
	}
	
	public List<IgnoredColumn> getIgnoredColumns() {
		return ignoredColumns;
	}
	
	public void setIgnoredColumns(List<IgnoredColumn> ignoredColumns) {
		this.ignoredColumns = ignoredColumns;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
}
