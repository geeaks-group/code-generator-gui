package com.geeaks.generator;

import com.geeaks.generator.controller.MainUIController;
import com.geeaks.generator.util.ConfigHelper;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @Description: 入口
 * @author gaoxiang
 * @date 2019-04-12 14:53:03
 */ 
public class MainUI extends Application {
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		ConfigHelper.createEmptyFiles();
		FXMLLoader fxmlLoader = new FXMLLoader(Thread.currentThread().getContextClassLoader().getResource("fxml/MainUI.fxml"));
		primaryStage.setResizable(true);
		primaryStage.setTitle("简易代码生成器");
		primaryStage.setScene(new Scene(fxmlLoader.load()));
		primaryStage.show();
		MainUIController controller = fxmlLoader.getController();
		controller.setPrimaryStage(primaryStage);
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
